package parser

import (
	"io"
	"jason/lexer"
	"jason/tokens"
	"strconv"

	"golang.org/x/xerrors"
)

type Parser struct {
	l   *lexer.Lexer
	tok tokens.Token
}

func (p *Parser) Next() {
	p.tok = p.l.NextToken()
}

func New(input io.Reader) *Parser {
	p := &Parser{l: lexer.New(input)}
	p.Next()
	return p
}

func (p *Parser) ParseLiteral() (interface{}, error) {
	switch p.tok.Type {
	case tokens.TokenInt:
		return parseInt(p.tok.Value)
	case tokens.TokenFloat:
		return parseFloat(p.tok.Value)
	case tokens.TokenString:
		return p.tok.Value, nil
	case tokens.TokenNull:
		return nil, nil
	case tokens.TokenBool:
		return parseBool(p.tok.Value)
	default:
		return nil, xerrors.Errorf("unexpected token: %q", p.tok)
	}
}

func (p *Parser) Parse() (interface{}, error) {
	var res interface{}
	var err error

	switch p.tok.Type {
	case tokens.TokenLBrack:
		return p.parseArray()
	case tokens.TokenLCurly:
		return p.parseObject()
	default:
		res, err = p.ParseLiteral()
	}

	return res, err
}

func parseInt(value string) (int64, error) {
	return strconv.ParseInt(value, 10, 64)
}

func parseFloat(value string) (float64, error) {
	return strconv.ParseFloat(value, 64)
}

func parseBool(value string) (bool, error) {
	switch value {
	case "true":
		return true, nil
	case "false":
		return false, nil
	default:
		return false, xerrors.Errorf("expected a bool, got %q", value)
	}
}

func (p *Parser) parseArray() ([]interface{}, error) {
	var result []interface{}
	for {
		p.Next()
		if p.tok.Type == tokens.TokenRBrack {
			break
		}
		obj, err := p.Parse()
		if err != nil {
			return nil, err
		}
		result = append(result, obj)
		p.Next()
		if p.tok.Type == tokens.TokenComma {
			continue
		}
		if p.tok.Type == tokens.TokenRBrack {
			break
		}
		return nil, xerrors.New("unexpected end of array")
	}
	return result, nil
}

func (p *Parser) parseObject() (map[string]interface{}, error) {
	result := make(map[string]interface{})
	for {
		p.Next()
		if p.tok.Type == tokens.TokenRCurly {
			break
		}
		if p.tok.Type != tokens.TokenString {
			return nil, xerrors.Errorf("keys must be strings; got %q", p.tok)
		}
		key := p.tok.Value

		p.Next()
		if p.tok.Type != tokens.TokenColon {
			return nil, xerrors.Errorf("expected ':'; got %q", p.tok)
		}

		p.Next()
		obj, err := p.Parse()
		if err != nil {
			return nil, xerrors.Errorf("could not find object value: %w", err)
		}
		result[key] = obj

		p.Next()
		if p.tok.Type == tokens.TokenComma {
			continue
		}
		if p.tok.Type == tokens.TokenRCurly {
			break
		}
		return nil, xerrors.New("unexpected end of object")
	}
	return result, nil
}
