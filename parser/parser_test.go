package parser_test

import (
	"fmt"
	"jason/parser"
	"reflect"
	"strings"
	"testing"
)

func TestParser(t *testing.T) {
	tt := []struct {
		input  string
		output interface{}
	}{
		{input: `"hello"`, output: "hello"},
		{input: `25`, output: int64(25)},
		{input: `-5`, output: int64(-5)},
		{input: `null`, output: nil},
		{input: `25.0`, output: 25.0},
		{input: `-.25`, output: -.25},
		{input: `true`, output: true},
		{input: `false`, output: false},
		{input: `[1, 2, 3]`, output: []interface{}{int64(1), int64(2), int64(3)}},
		{input: `{"one": 1, "two": 2}`, output: map[string]interface{}{"one": int64(1), "two": int64(2)}},
		{input: `{"one": 1, "two": 2, "three": [4, -4.4, false, null]}`,
			output: map[string]interface{}{"one": int64(1), "two": int64(2), "three": []interface{}{int64(4), -4.4, false, nil}}},
	}

	for idx, tc := range tt {
		t.Run(fmt.Sprintf("Test %d", idx+1), func(t *testing.T) {
			p := parser.New(strings.NewReader(tc.input))

			res, err := p.Parse()
			if err != nil {
				t.Fatalf("could not parse json: %q because %q", tc.input, err)
			}
			if !reflect.DeepEqual(res, tc.output) {
				t.Fatalf("expected %[1]T(%[1]v), got %[2]T(%[1]v)", tc.output, res)
			}
		})
	}
}
