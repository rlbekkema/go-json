package main

import (
	"bufio"
	"jason/parser"
	"jason/pretty"
	"log"
	"os"
	"strings"
)

func main() {
	var jsn string
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		jsn += scanner.Text()
	}
	log.Printf("Value: %q\n", jsn)

	l := parser.New(strings.NewReader(jsn))
	obj, err := l.Parse()
	if err != nil {
		log.Fatalf("could not parse this json: %+v\n", err)
	}
	log.Printf("result: %v\n", obj)

	log.Println("Pretty:")
	pp := pretty.New(strings.NewReader(jsn))
	pp.Display(os.Stdout)
}
