package lexer

import (
	"bufio"
	"io"
	"jason/tokens"
	"strings"
	"unicode"

	"golang.org/x/xerrors"
)

type Lexer struct {
	source *bufio.Reader
	err    error
}

func New(source io.Reader) *Lexer {
	l := &Lexer{
		source: bufio.NewReader(source),
	}
	return l
}

func (s *Lexer) nextRune() rune {
	var r rune
	var err error
	// Read until non-whitespace char, return that
	for {
		r, _, err = s.source.ReadRune()
		if err != nil {
			s.err = err
			return 0
		}
		if unicode.IsSpace(r) {
			continue
		}
		return r
	}
}

func (s *Lexer) unreadRune() {
	err := s.source.UnreadRune()
	if err != nil {
		s.err = err
	}
}

func (s *Lexer) readString() tokens.Token {
	var str strings.Builder
	for {
		r := s.nextRune()
		switch {
		case r == '"':
			return tokens.NewToken(tokens.TokenString, str.String())
		case r == '\\':
			if r := s.nextRune(); r == '"' {
				str.WriteRune('"')
			} else {
				str.WriteRune('\\')
				s.unreadRune()
			}
		default:
			str.WriteRune(r)
		}
	}
}

func (s *Lexer) readWord(typ tokens.TokenType, word string) tokens.Token {
	s.unreadRune()
	buf := make([]byte, len(word))
	num, err := s.source.Read(buf)
	if err != nil {
		s.err = xerrors.Errorf("could not find word: %w", err)
	}
	if num != len(word) {
		s.err = xerrors.Errorf("expected %q; got %q", word, string(buf))
	}
	return tokens.NewToken(typ, string(buf))
}

func (s *Lexer) readNumber() tokens.Token {
	s.unreadRune()
	var nr strings.Builder
	token := tokens.TokenInt
	for r := s.nextRune(); r >= '0' && r <= '9' || r == '.' || r == '-'; r = s.nextRune() {
		if r == '.' {
			token = tokens.TokenFloat
		}
		nr.WriteRune(r)
	}
	s.unreadRune()
	return tokens.NewToken(token, nr.String())
}

func (s *Lexer) NextToken() tokens.Token {
	r := s.nextRune()

	switch r {
	case '"':
		return s.readString()
	case '[':
		return tokens.NewToken(tokens.TokenLBrack, string(r))
	case ']':
		return tokens.NewToken(tokens.TokenRBrack, string(r))
	case ',':
		return tokens.NewToken(tokens.TokenComma, string(r))
	case '{':
		return tokens.NewToken(tokens.TokenLCurly, string(r))
	case '}':
		return tokens.NewToken(tokens.TokenRCurly, string(r))
	case ':':
		return tokens.NewToken(tokens.TokenColon, string(r))
	case 't':
		return s.readWord(tokens.TokenBool, "true")
	case 'f':
		return s.readWord(tokens.TokenBool, "false")
	case 'n':
		return s.readWord(tokens.TokenNull, "null")
	case 0:
		return tokens.NewToken(tokens.TokenEOF, "")
	default:
		if r >= '0' && r <= '9' || r == '.' || r == '-' {
			return s.readNumber()
		}
		return tokens.NewToken(tokens.TokenIllegal, string(r))
	}
}
