package lexer_test

import (
	"jason/lexer"
	"jason/tokens"
	"strings"
	"testing"
)

func TestLexer(t *testing.T) {
	inputJSON := `[1, {"one": -5.5}, 1.1, -3, true, false, null]`
	tt := []struct {
		typ   tokens.TokenType
		value string
	}{
		{typ: tokens.TokenLBrack, value: "["},
		{typ: tokens.TokenInt, value: "1"},
		{typ: tokens.TokenComma, value: ","},
		{typ: tokens.TokenLCurly, value: "{"},
		{typ: tokens.TokenString, value: "one"},
		{typ: tokens.TokenColon, value: ":"},
		{typ: tokens.TokenFloat, value: "-5.5"},
		{typ: tokens.TokenRCurly, value: "}"},
		{typ: tokens.TokenComma, value: ","},
		{typ: tokens.TokenFloat, value: "1.1"},
		{typ: tokens.TokenComma, value: ","},
		{typ: tokens.TokenInt, value: "-3"},
		{typ: tokens.TokenComma, value: ","},
		{typ: tokens.TokenBool, value: "true"},
		{typ: tokens.TokenComma, value: ","},
		{typ: tokens.TokenBool, value: "false"},
		{typ: tokens.TokenComma, value: ","},
		{typ: tokens.TokenNull, value: "null"},
		{typ: tokens.TokenRBrack, value: "]"},
		{typ: tokens.TokenEOF, value: ""},
	}

	l := lexer.New(strings.NewReader(inputJSON))
	for idx, tc := range tt {
		tok := l.NextToken()
		if tok.Type != tc.typ {
			t.Fatalf("test: %d: expected type %q, got %q", idx+1, tc.typ, tok.Type)
		}
		if tok.Value != tc.value {
			t.Fatalf("test: %d: expected value %q, got %q", idx+1, tc.value, tok.Value)
		}
	}
}
