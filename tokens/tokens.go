package tokens

import "fmt"

type TokenType int

const (
	TokenEOF TokenType = iota
	TokenIllegal
	TokenInt
	TokenFloat
	TokenString
	TokenBool
	TokenNull
	TokenLBrack
	TokenRBrack
	TokenLCurly
	TokenRCurly
	TokenComma
	TokenColon
)

var tokenNames = map[TokenType]string{
	TokenEOF:     "EOF",
	TokenIllegal: "Illegal",
	TokenInt:     "Int",
	TokenFloat:   "Float",
	TokenString:  "String",
	TokenBool:    "Bool",
	TokenNull:    "Null",
	TokenLBrack:  "Left bracket",
	TokenRBrack:  "Right bracket",
	TokenLCurly:  "Left curly",
	TokenRCurly:  "Right curly",
	TokenComma:   "Comma",
	TokenColon:   "Colon",
}

func (t TokenType) String() string {
	name, ok := tokenNames[t]
	if !ok {
		name = "Unknown"
	}
	return name
}

type Token struct {
	Type  TokenType
	Value string
}

func NewToken(typ TokenType, value string) Token {
	return Token{typ, value}
}

func (t Token) String() string {
	return fmt.Sprintf("%s: %s", t.Type, t.Value)
}
