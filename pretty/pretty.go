package pretty

import (
	"io"
	"jason/lexer"
	"jason/tokens"
	"strings"
)

type Pretty struct {
	l   *lexer.Lexer
	tok tokens.Token
}

func (p *Pretty) Next() {
	p.tok = p.l.NextToken()
}

func New(input io.Reader) *Pretty {
	p := Pretty{l: lexer.New(input)}

	return &p
}

func (p *Pretty) Display(w io.Writer) {
	write := func(out string) {
		w.Write([]byte(out))
	}

	indent := 0
	skipNewline := false
	for {
		if p.tok.Type == tokens.TokenRBrack || p.tok.Type == tokens.TokenRCurly {
			indent -= 4
		}

		if p.tok.Type != tokens.TokenColon && p.tok.Type != tokens.TokenComma && !skipNewline {
			write("\n")
			write(strings.Repeat(" ", indent))
		}

		skipNewline = p.tok.Type == tokens.TokenColon
		if skipNewline {
			write(" ")
		}

		write(p.tok.Value)

		if p.tok.Type == tokens.TokenLBrack || p.tok.Type == tokens.TokenLCurly {
			indent += 4
		}

		p.Next()
		if p.tok.Type == tokens.TokenEOF {
			break
		}
	}
	write("\n")
}
